class intSet(object):
    """An intSet is a set of integers
    The value is represented by a list of ints, self.vals.
    Each int in the set occurs in self.vals exactly once."""

    def __init__(self):
        """Create an empty set of integers"""
        self.vals = []

    def insert(self, e):
        """Assumes e is an integer and inserts e into self""" 
        if not e in self.vals:
            self.vals.append(e)

    def member(self, e):
        """Assumes e is an integer
           Returns True if e is in self, and False otherwise"""
        return e in self.vals

    def remove(self, e):
        """Assumes e is an integer and removes e from self
           Raises ValueError if e is not in self"""
        try:
            self.vals.remove(e)
        except:
            raise ValueError(str(e) + ' not found')

    def __str__(self):
        """Returns a string representation of self"""
        self.vals.sort()
        return '{' + ','.join([str(e) for e in self.vals]) + '}'
        
    def intersect(self, other):
        int_set_return = intSet()
        for i in self.vals:
            if other.member(i):
                int_set_return.insert(i)
    
    def __len__(self):
        return len(self.vals)

s1 = intSet()
s2 = intSet()

for i in range(20):
    s1.insert(i)

for j in range(0, 40, 2):
    s2.insert(j)

print("len(s1) = {}".format(len(s1)))
print("len(s2) = {}".format(len(s2)))

s3 = s1.intersect(s2)
