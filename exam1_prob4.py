def dotProduct(listA, listB):
	product = 0
	for i in range(len(listA)):
		product += listA[i]*listB[i]
	return product

print(dotProduct([1, 2, 3], [4, 5, 6]))
