def rem(x, a):
	print("{} {}".format(x, a))
	if x == a:
		return 0
	elif x < a:
		return x
	else:
		return rem(x-a, a)

def f(n):
	print(n)
	if n == 0:
		return 1
	else:
		return n * f(n-1)

print(f(3))