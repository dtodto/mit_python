def oddTuples(aTup):
	temp = ()
	for i in range(len(aTup)):
		if (i+1)%2 is not 0:
			temp = temp + (aTup[i], )
	return temp
test = oddTuples(('I', 'am', 'a', 'test', 'tuple'))

print(test)