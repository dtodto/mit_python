def myLog(x, b):
	power_guess = x
	while b**power_guess > x:
		power_guess -= 1
	return power_guess

print(myLog(16, 2))
print(myLog(15, 3))