from math import tan, pi

def polysum(n, s):
	def area(n=n ,s=s):
		return (0.25*n*s**2)/tan(pi/n)
	def boundry(n=n, s=s):
		return n*s
	return round((area()+boundry()**2), 4)

test = polysum(int(input("enter n: ")), int(input("enter s: ")))
print(test)