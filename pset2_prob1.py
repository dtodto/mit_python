def calc_card_balance(balance, annualInterestRate, monthlyPaymentRate, month):
	if month >= 12:
		return balance
	else:
		balance = balance - balance*monthlyPaymentRate
		balance = balance + balance * annualInterestRate/12
		balance = round(balance, 2) - 0.01
		return calc_card_balance(balance, annualInterestRate, monthlyPaymentRate,month+1)

print(calc_card_balance(int(input("enter balance: ")),float(input("enter annualInterestRate: ")), float(input("enter monthlyPaymentRate: ")), 0))