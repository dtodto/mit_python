animals = {'a': ['aardvark'], 'b': ['baboon'], 'c': ['coati']}
animals['d'] = ['donkey']
animals['d'].append('dog')
animals['d'].append('dingo')

def how_many(aDict):
	"""
	returns how many valuse aDict holds
	"""
	how_many = 0
	for key in aDict.keys():
		how_many += len(aDict[key])
	return how_many

def biggest(aDict):
	"""
	returns key with largest value assosiated with it
	"""
	biggest = 0
	key_biggest =''
	for key in aDict.keys():
		if len(aDict[key]) > biggest:
			biggest = len(aDict[key])
			key_biggest = key
	return key_biggest



print(biggest(animals))