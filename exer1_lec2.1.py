def iterPower(base, exp):
	if exp is 0:
		return 1
	else:
		return base * iterPower(base, exp-1)

print(iterPower(2,8))