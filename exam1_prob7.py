def applyF_filterG(L, f, g):
	#L is list
	#f and g are defined functions
	#f takes integer applies function and returns integer
	#g takes in an integer applies Boolean function, returns either True or False
	#Mutates L such that, for i in L, L contains i if g(f(i)) returns True and no other elements
	#returns the largest element in the mutated L or -1 if list is empty
	aList = L.copy()
	L.clear()
	for i in aList:
		if g(f(i)):
			L.append(i)
	if len(L) is 0:
		return -1
	else:
		L.sort()
		return L[len(L)-1]
def f(i):
	return i+2
def g(i):
	return i>5

L = []
print(applyF_filterG(L, f, g))
print(L)
