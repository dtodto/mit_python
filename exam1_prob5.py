def uniqueValues(aDict):
	keys=[]
	values=[]
	not_unique=[]
	for i in aDict.keys():
		not_unique.append(aDict[i])
		if aDict[i] not in values:
			values.append(aDict[i])
			keys.append(i)
	temp = []
	for i in values:
		if not(not_unique.count(i) > 1):
			temp.append(keys[values.index(i)])
	return temp

print(uniqueValues({1: 1, 2: 1, 3: 1, 4: 5, 5: 7, 6: 10}))